//
//  SessionContainer.m
//  Chatter
//
//  Copyright © 2016 Cole Cummings. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MultipeerConnectivity/MultipeerConnectivity.h>

@interface SessionContainer : NSObject

@property MCPeerID* peerID;
@property MCSession* sessionMain;
@property MCNearbyServiceBrowser* browser;
@property MCNearbyServiceAdvertiser* advertiser;
@property NSMutableArray<MCPeerID*>* foundPeers;
//@property invitationHandler: ((boolean, MCSession!)->Void)!
@property NSString* timeCreated;

@end

@protocol SessionContainerDelegate
// list of methods and properties
@end

@implementation SessionContainer
//try moving these into init method
//foundPeers = [NSMutableArray array];
//timeCreated = [NSDate date];

@end

//var timeCreated = ["Time" : "\(NSDate().timeIntervalSince1970 * 1000)"]
