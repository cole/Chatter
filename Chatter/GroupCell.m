//
//  GroupCell.m
//  Chatter
//
//  Created by Cole Cummings on 3/20/16.
//  Copyright © 2016 Cole Cummings. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface GroupCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *groupButton;

@end


@implementation GroupCell

@end
