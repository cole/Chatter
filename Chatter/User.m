//
//  User.m
//  Chatter
//
//  Copyright © 2016 Cole Cummings. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface User : NSObject

@property NSString* username;
@property NSString* id;

@end


@implementation User

@end
