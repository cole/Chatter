//
//  ChatCell.m
//  Chatter
//
//  Created by Cole Cummings on 3/20/16.
//  Copyright © 2016 Cole Cummings. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface ChatCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *senderLabel;
@property (weak, nonatomic) IBOutlet UITextView *message;

@end


@implementation ChatCell

@end
