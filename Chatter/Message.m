//
//  Message.m
//  Chatter
//
//  Copyright © 2016 Cole Cummings. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : NSObject

@property NSString* summary;
@property NSString* from;

@end



@implementation Message

@end
