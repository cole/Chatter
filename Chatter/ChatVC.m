//
//  ChatVC.m
//  Chatter
//
//  Created by Cole Cummings on 3/9/16.
//  Copyright © 2016 Cole Cummings. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>
#import <MultipeerConnectivity/MultipeerConnectivity.h>
#import "AppDelegate.h"


@interface ChatVC : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate, MessageDelagate, UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITableView *chatTableView;
@property (weak, nonatomic) IBOutlet UITextField *messageBox;




@end

@implementation ChatVC
//In the previous app we retrieved this here, use this code to grab the appDelegate at run time
//AppDelegate* sharedApp = [AppDelegate sharedAppdelegate];

- (void)viewDidLoad {
    [super viewDidLoad];
    //globalVariables.sharedInstance.messageHandler?.delegate = self
    //--these errors should go away once this class conforms to their API
    self.chatTableView.delegate = self;
    self.chatTableView.dataSource = self;
    self.messageBox.delegate = self;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end

//MARK -everything below here is swift code to be translated

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillShow:"), name:UIKeyboardWillShowNotification, object: nil);
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("keyboardWillHide:"), name:UIKeyboardWillHideNotification, object: nil);
        
    }

    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return globalVariables.sharedInstance.peers.getMessages(globalVariables.sharedInstance.currentChatter!).count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        // Table view cells are reused and should be dequeued using a cell identifier.
        let cell = self.chatTableView.dequeueReusableCellWithIdentifier("ChatCell", forIndexPath: indexPath) as! ChatCell
        
        //    self.sendMessageButton.enabled = NO;
        let message = globalVariables.sharedInstance.peers.getMessages(globalVariables.sharedInstance.currentChatter!)[indexPath.row]
        
        cell.senderLabel?.text = message.from
        cell.message?.text = message.summary
        
        return cell
    }
    
    func messageButton() {
        if(!messageBox.text.isEmpty) {
            let fetchRequest = NSFetchRequest(entityName: "User")
            do {
                let fetchedEntities = try managedObjectContext.executeFetchRequest(fetchRequest) as! [User]
                let send = fetchedEntities.first
                // globalVariables.sharedInstance.messageHandler?.sendMessage(globalVariables.sharedInstance.currentChatter!, message: messageBox.text)
                globalVariables.sharedInstance.peers.addMessages(globalVariables.sharedInstance.currentChatter!, msg: messageBox.text, us: true)
                addMessage(send!.username, Msg: messageBox.text)
                if(globalVariables.sharedInstance.inGroup){
                    globalVariables.sharedInstance.messageHandler?.sendGroupMessage(globalVariables.sharedInstance.currentChatter!, message: messageBox.text)
                }else{
                    globalVariables.sharedInstance.messageHandler?.sendMessage(globalVariables.sharedInstance.currentChatter!, message: messageBox.text)
                }
            } catch {
                //I don't know what to do here
            }
        }
    }
    
    func addMessage(sender: String, Msg: String) {
        let message = Message.init(summary: Msg, from: sender)
        self.chatTableView.reloadData()
    }
    
    func addTestMessage(Msg: String) {
        let fetchRequest = NSFetchRequest(entityName: "User")
        do {
            let fetchedEntities = try managedObjectContext.executeFetchRequest(fetchRequest) as! [User]
            let send = fetchedEntities.first
            let message = Message.init(summary: Msg, from: send!.username)
            self.chatTableView.reloadData()
            
        } catch {
            // You're screwed
        }
        
    }
    
    @IBAction func sendButton(sender: UIButton) {
        //TODO confirm that the message is recieved
        messageButton();
        scrollToLastRow()
        messageBox.text = ""
    }
    
    func scrollToLastRow() {
        let indexPath = NSIndexPath(forRow: globalVariables.sharedInstance.peers.getMessages(globalVariables.sharedInstance.currentChatter!).count - 1, inSection: 0)
        self.chatTableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: .Bottom, animated: true)
    }
    
    func textFieldShouldReturn(messageBox: UITextField) -> Bool // called when 'return' key pressed
    {
        messageBox.resignFirstResponder()
        messageButton()
        return true;
    }
    
    @IBAction func backButton(sender: UIButton) {
        performSegueWithIdentifier("chatToWelcome", sender: nil)
        
    }
    
    func textView(textView: UITextView, shouldChangeTextInRange range: NSRange, replacementText text: String) -> Bool {
        if(text == "\n") {
            textView.resignFirstResponder()
            messageButton()
            scrollToLastRow()
            messageBox.text = ""
            return false
        }
        return true
    }
    
    
    // MARK: trying to keep keyboard from covering the textbox but it doesn't work very well right now on simulator
    func keyboardWillShow(sender: NSNotification) {
        self.view.frame.origin.y -= 264
    }
    
    func keyboardWillHide(sender: NSNotification) {
        self.view.frame.origin.y += 264
        //150 is the height on phones
    }
    func recievedMsg(from: String, message : String) {
        addMessage(from, Msg: message)
        scrollToLastRow()
    }
    
    /*
     // MARK: - Navigation
     
     // In a storyboard-based application, you will often want to do a little preparation before navigation
     override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
     // Get the new view controller using segue.destinationViewController.
     // Pass the selected object to the new view controller.
     }
     */
    
}
