//
//  GlobalVariables.m
//  Chatter
//
//  Created by Cole Cummings on 3/28/16.
//  Copyright © 2016 Cole Cummings. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MultipeerConnectivity/MultipeerConnectivity.h>

@interface GlobalVariables : NSObject {
    
}
//commented are unimplemented classes
//@property pathHandler *paths;
//@property Peers *peers;
//@property SessionContainer *session;
@property MCPeerID *peerID;
//@property MessageHandler *messageHandler;
@property bool active;
@property NSString *currentChatter;
@property NSString *ourName;
@property NSMutableArray *groups; //Array of Strings at declaration
@property NSMutableArray *allGroups;
@property bool inGroup;

//+(id)sharedManager;
-(void)start:(MCPeerID*) name;
-(void)changePeerID:(MCPeerID *)id;

@end


@implementation GlobalVariables

@synthesize active;
@synthesize inGroup;
@synthesize allGroups;
@synthesize groups;
@synthesize peerID;
@synthesize currentChatter;

void changePeerID(MCPeerID*);
void createSession();
void createMessageHandler();
void startP();


+ (id)sharedManager {
    static GlobalVariables *sharedMyManager = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedMyManager = [[self alloc] init];
    });
    return sharedMyManager;
}

- (id)init {
    if (self = [super init]) {
        active = false;
        inGroup = false;
    }
    return self;
}

- (void) start:(MCPeerID*) name {
    if(!active){
        active = true;
        changePeerID(name);
        createSession();
        createMessageHandler();
        startP();
        //Peers not implemented yet
        peers.startP();
    }
}

-(void) end {
    active = false;
}

-(void) addAllGroup:(NSString*) group {
    if ( ![allGroups containsObject: group] ) {
        [allGroups addObject:group];
    }
}

-(void) addGroup:(NSString*) group {
    if ( ![groups containsObject: group] ) {
        [groups addObject:group];
    }
}

-(void) changePeerID:(MCPeerID *)id {
    peerID = id;
}

-(void) createSession {
    session = SessionContainer();
}

-(void) createMessageHandler {
    messageHandler = MessageHandler();
}

-(void) changeCurrentChatter:(NSString*) id {
    currentChatter = id;
}

@end


//MARK everything below here is Swift to be converted

class globalVariables {
    
    static let sharedInstance = globalVariables()
    
    private func startP(){
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            // do some task
            while(self.active){
                self.messageHandler!.heartbeat()
                NSThread.sleepForTimeInterval(5)
            }
        }
    }
    
}