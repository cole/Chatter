//
//  main.m
//  Chatter
//
//  Created by Cole Cummings on 2/14/16.
//  Copyright © 2016 Cole Cummings. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
