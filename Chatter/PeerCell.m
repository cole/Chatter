//
//  PeerCell.m
//  Chatter
//
//  Created by Cole Cummings on 3/20/16.
//  Copyright © 2016 Cole Cummings. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface PeerCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *peerButton;

@end


@implementation PeerCell

@end
