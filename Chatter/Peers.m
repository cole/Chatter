//
//  Peers.m
//  Chatter
//
//  Created by Cole Cummings on 4/18/16.
//  Copyright © 2016 Cole Cummings. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@protocol peerDelegate
@required
NSMutableArray* changedPeers();
@optional
// list of optional methods
@end

@interface Peers : NSObject

@property NSMutableArray* peerList;
@property NSMutableArray* deletePeers;
//@property peerDelegate delegate;
@property NSMutableArray* messages;

@end

//Use this for deletePeers
@interface dPeer : NSObject
{
    NSString *peer;
    BOOL      delete;
}
@property (nonatomic, assign) NSString* peer;
@property (nonatomic, assign) BOOL delete;
@end

//Use this for messages
@interface messages : NSObject
{
    NSString *name;
    NSString *message;
}
@property (nonatomic, assign) NSString* name;
@property (nonatomic, assign) NSString* message;
@end

@implementation Peers

@synthesize peerList;
@synthesize deletePeers;
//@synthesize peerDelegate;

NSMutableArray* peerList;
NSMutableArray* deletePeers;

- (id)init {
    if (self = [super init]) {
        peerList = [NSMutableArray alloc];
    }
    return self;
}

void addPeer(NSString *peer) {
    if (![peerList containsObject:peer])
    {
        [peerList addObject:peer];
        dPeer *d = [dPeer alloc];
        d.delete = false;
        d.peer = peer;
        [deletePeers addObject:d];
        //delegate?.changedPeers(); no idea what were doing with this
    }
}

BOOL hasPeer(NSString* peer) {
    return [peerList containsObject:peer];
}

void addMessages(NSString* peer, NSString* msg, BOOL us) {
    if()
}

@end


//Below here is Swift code to be translated


class Peers {
    
    func addMessages(peer : String, msg : String, us : Bool){
        if(messages[peer] == nil){
            messages[peer] = []
        }
        if(us){
            var mm = Message.init(summary: msg, from: globalVariables.sharedInstance.ourName)
            messages[peer]?.append(mm)
        }else{
            var mm = Message.init(summary: msg, from: peer)
            messages[peer]?.append(mm)
        }
    }
    
    func addGroupMessages(group : String, peer : String, msg : String, us : Bool){
        if(messages[group] == nil){
            messages[group] = []
        }
        if(us){
            var mm = Message.init(summary: msg, from: globalVariables.sharedInstance.ourName)
            messages[group]?.append(mm)
        }else{
            var mm = Message.init(summary: msg, from: peer)
            messages[group]?.append(mm)
        }
    }
    
    func getMessages(peer : String) -> [Message]{
        if(messages[peer] == nil){
            messages[peer] = []
        }
        return messages[peer]!
    }
    
    func startP(){
        let priority = DISPATCH_QUEUE_PRIORITY_DEFAULT
        dispatch_async(dispatch_get_global_queue(priority, 0)) {
            // do some task
            while(globalVariables.sharedInstance.active){
                for shouldDelete in self.deletePeers.keys{
                    if(self.deletePeers[shouldDelete] == true){
                        self.removePeer(shouldDelete)
                        self.deletePeers[shouldDelete] = nil
                    }else{
                        self.deletePeers[shouldDelete] = true
                    }
                }
                NSThread.sleepForTimeInterval(12)
            }
        }
    }
    
    func getSize()-> Int{
        return peers.count
    }
    
    func removePeer(peer : String){
        var i = 0
        for items in peers{
            if(items == peer){
                peers.removeAtIndex(i)
                delegate?.changedPeers()
            }
            i += 1
        }
    }
    
    func getPeers() -> [String] {
        return peers
    }
    
}