//
//  WelcomeVC.m
//  Chatter
//
//  Created by Cole Cummings on 3/1/16.
//  Copyright © 2016 Cole Cummings. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface WelcomeVC : UIViewController

@end

@interface WelcomeVC ()

@end

@implementation WelcomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)signIn:(id)sender {
    
    [self performSegueWithIdentifier:@"welcomeSeg" sender:sender];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
